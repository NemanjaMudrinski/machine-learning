import pymongo

from centers import centers

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["openstreetmap"]
germany_nodes = mydb["germany_nodes"]
germany_streets_coordinates = mydb["germany_streets_coordinates"]
germany_buildings_coordinates = mydb["germany_buildings_coordinates"]

""" streets = []
buildings = [] """

for node in germany_nodes.find():
    keys = node.keys()
    if "tag" in keys:
        for tag in node["tag"]:
            if isinstance(tag, str) and node["tag"]["k"] == "addr:street":
                """ streets.append(
                    {
                        "name": node["tag"]["v"],
                        "location": {
                            "type": "Point",
                            "coordinates": [float(node["lon"]), float(node["lat"])]
                        }
                    }
                ) """
                germany_streets_coordinates.insert_one({
                    "name": node["tag"]["v"],
                    "location": {
                        "type": "Point",
                        "coordinates": [float(node["lon"]), float(node["lat"])]
                    }
                })
            elif not isinstance(tag, str) and tag["k"] == "addr:street":
                """ streets.append(
                    {
                        "name": tag["v"],
                        "location": {
                            "type": "Point",
                            "coordinates": [float(node["lon"]), float(node["lat"])]
                        }
                    }
                ) """
                germany_streets_coordinates.insert_one({
                    "name": tag["v"],
                    "location": {
                        "type": "Point",
                        "coordinates": [float(node["lon"]), float(node["lat"])]
                    }
                })
            if isinstance(tag, str) and node["tag"]["k"] == "building":
                """ buildings.append(
                    {
                        "name": node["tag"]["v"],
                        "location": {
                            "type": "Point",
                            "coordinates": [float(node["lon"]), float(node["lat"])]
                        }
                    }
                ) """
                germany_buildings_coordinates.insert_one({
                    "name": node["tag"]["v"],
                    "location": {
                        "type": "Point",
                        "coordinates": [float(node["lon"]), float(node["lat"])]
                    }
                })
            elif not isinstance(tag, str) and tag["k"] == "building":
                tag_building = tag
                name_exists = False
                for t in node["tag"]:
                    if t["k"]=="name":
                        name_exists = True
                        """ buildings.append(
                            {
                                "name": t["v"],
                                "location": {
                                    "type": "Point",
                                    "coordinates": [float(node["lon"]), float(node["lat"])]
                                }
                            }
                        ) """
                        germany_buildings_coordinates.insert_one({
                            "name": t["v"],
                            "location": {
                                "type": "Point",
                                "coordinates": [float(node["lon"]), float(node["lat"])]
                            }
                        })
                        break
                if not name_exists:
                    """ buildings.append(
                        {
                            "name": tag_building["v"],
                            "location": {
                                "type": "Point",
                                "coordinates": [float(node["lon"]), float(node["lat"])]
                            }
                        }
                    ) """
                    germany_buildings_coordinates.insert_one({
                        "name": tag_building["v"],
                        "location": {
                            "type": "Point",
                            "coordinates": [float(node["lon"]), float(node["lat"])]
                        }
                    })

# ALL COORDINATES
""" germany_streets_coordinates.insert_many(streets)
germany_buildings_coordinates.insert_many(buildings) """

# CENTERS
""" germany_streets_coordinates.insert_many(centers(streets))
germany_buildings_coordinates.insert_many(centers(buildings)) """

germany_streets_coordinates.create_index([("location", pymongo.GEOSPHERE)])
germany_buildings_coordinates.create_index([("location", pymongo.GEOSPHERE)])
