OpenStreetMap baza Njemačke na linku: https://download.geofabrik.de/europe/germany-latest.osm.bz2
To je arhiva u kojoj se nalazi fajl.
Taj fajl je OSM XML i putanja do tog fajla je inicijalno podešena tako da treba da se nalazi u folderu "osm to db".

Skripta za učitavanje OSM fajla u MongoDB je load_osm_to_db.py
Ona u bazu upisuje 4 kolekcije koje su zapravo komponente OSM fajla, a to su: bounds, nodes, ways i relations.

Skripta streets_and_buildings.py čita sve node-ove iz baze, izdvaja ulice i objekte odnosno node-ove koji imaju tag "addr:street" i "building",
a zatim upisuje njihove centre u bazu kao GeoJSON objekte.
GeoJSON objekti se sastoje od imena ulice, odnosno objekta i lokacije, tj. koordinata (longituda i latituda).
Ne upisuju se svi node-ovi već samo njihovi centri zato što se jedna ista ulica, odnosno objekat sastoji iz više node-ova
i prilikom pretraživanja mape u nekom radijusu dobili bismo duplikate ulice tj. objekta, odnosno dobili bismo sve node-ove koji opisuju zapravo istu ulicu, tj. objekat.

Računanje centara node-ova vrši skripta centers.py tako što izračuna prosjek longituda i latituda node-ova.
Kolekcije ulica i objekata u bazi se indeksiraju po lokaciji indeksom "2dsphere" radi bržeg pretraživanja.

I konačno, skripta streets_and_buildings_radius.py izdvaja ulice i objekate iz baze koji se nalaze u proizvoljno zadatom radijusu proizvoljno izabrane tačke na mapi.
Izdvojene ulice i objekte zapisuje u JSON fajlove "streets_near_point.json", odnosno  "buildings_near_point.json".