import json

import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["openstreetmap"]
germany_nodes = mydb["germany_nodes"]
germany_streets_coordinates = mydb["germany_streets_coordinates"]
germany_buildings_coordinates = mydb["germany_buildings_coordinates"]


def find_nearest(collection, search_point_coordinates, distance):
    streets_near_point_cursor = collection.find(
        {
            "location":
                {"$near":
                    {
                        "$geometry": {"type": "Point",  "coordinates": search_point_coordinates},
                        "$maxDistance": distance
                    }
                 }
        },
        {
            "_id": 0,
            "name": 1
        }
    )

    streets_near_point = []
    for street in streets_near_point_cursor:
        if street not in streets_near_point:
            streets_near_point.append(street)

    return streets_near_point


""" # trazeno mjesto iz zadatka - nije pronasao buildings jer node-ovi nemaju tag building
search_point_coordinates = [11.571041, 48.250536]
distance = 3000 """

# npr. centar Minhena - imamo i streets i buildings
search_point_coordinates = [11.57557, 48.13723]
distance = 500

with open("streets_near_point.json", "w", encoding="utf-8") as f:
    json.dump(find_nearest(germany_streets_coordinates,
                           search_point_coordinates, distance), f, ensure_ascii=False)

with open("buildings_near_point.json", "w", encoding="utf-8") as f:
    json.dump(find_nearest(germany_buildings_coordinates,
                           search_point_coordinates, distance), f, ensure_ascii=False)
