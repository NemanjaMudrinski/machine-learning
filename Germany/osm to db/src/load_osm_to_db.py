import xml.etree.ElementTree as ET
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["openstreetmap"]
germany_bounds = mydb["germany_bounds"]
germany_nodes = mydb["germany_nodes"]
germany_ways = mydb["germany_ways"]
germany_relations = mydb["germany_relations"]

filename = "./../germany-latest.osm"

for event, elem in ET.iterparse(filename):
    e = elem.attrib
    if len(list(elem))==1:
        for subelem in list(elem):
            e[subelem.tag] = subelem.attrib
    else:
        for subelem in list(elem):
            if subelem.tag not in e.keys():
                e[subelem.tag] = []
            else:
                e[subelem.tag].append(subelem.attrib)

    if elem.tag == "bounds":
        germany_bounds.insert_one(e)
    elif elem.tag == "node":
        germany_nodes.insert_one(e)
    elif elem.tag == "way":
        germany_ways.insert_one(e)
    elif elem.tag == "relation":
        germany_relations.insert_one(e)

    if event == 'end' and elem.tag in ['node', 'way', 'relation']:
        elem.clear()