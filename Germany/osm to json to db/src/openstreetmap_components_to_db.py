import json
import pymongo
from load_osm_data import load_osm_data

input_file_path = "/../germany-latest.osm"
output_folder_path = "/../germany-latest-json"
output_file_name = "germany-latest"

files_number, output_path = load_osm_data(input_file_path, output_folder_path, output_file_name)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["openstreetmap"]
germany_bounds = mydb["germany_bounds"]
germany_nodes = mydb["germany_nodes"]
germany_ways = mydb["germany_ways"]
germany_relations = mydb["germany_relations"]

for i in range(files_number):
    with open(output_path+str(i+1)+".json", mode="r", encoding="utf-8") as file:
        print(i+1)
        data = json.load(file)
        try:
            germany_bounds.insert_one(data["osm"]["bounds"])
        except KeyError:
            pass
        try:
            germany_nodes.insert_many(data["osm"]["node"])
        except KeyError:
            pass
        try:
            germany_ways.insert_many(data["osm"]["way"])
        except KeyError:
            pass
        try:
            germany_relations.insert_many(data["osm"]["relation"])
        except KeyError:
            pass
