import os
from osm_to_json import osm_to_json

def load_osm_data(input_file_path, output_folder_path, output_file_name):
    files_number = 0
    input_path = os.path.dirname(__file__) + input_file_path
    output_folder = os.path.dirname(__file__) + output_folder_path
    output_path = output_folder+"/"+output_file_name
    if os.path.exists(output_folder):
        path, dirs, files = next(os.walk(output_folder))
        files_number = len(files)
    if not os.path.exists(output_path+"1.json"):
        files_number = osm_to_json(input_path, output_path)
    return files_number, output_path