def centers(streets):
    streets_lons_lats = {}
    for street in streets:
        try:
            streets_lons_lats[street["name"]]["lons"].append(
                street["location"]["coordinates"][0])
            streets_lons_lats[street["name"]]["lats"].append(
                street["location"]["coordinates"][1])
        except Exception:
            streets_lons_lats[street["name"]] = {}
            streets_lons_lats[street["name"]]["lons"] = [
                street["location"]["coordinates"][0]]
            streets_lons_lats[street["name"]]["lats"] = [
                street["location"]["coordinates"][1]]
    streets_centers = []
    for street_name in streets_lons_lats.keys():
        streets_centers.append({
            "name": street_name,
            "location": {
                "type": "Point",
                "coordinates": [float(sum(streets_lons_lats[street_name]["lons"])/len(streets_lons_lats[street_name]["lons"])), float(sum(streets_lons_lats[street_name]["lats"])/len(streets_lons_lats[street_name]["lats"]))]
            }
        })

    return streets_centers
